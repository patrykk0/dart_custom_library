// Copyright (c) 2019 Patryk Kacperski

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import 'package:flutter/material.dart';

/// Bold text with a gray description underneath
class TextWithDescription extends StatelessWidget {

  final String _mainText;
  final String _descriptionText;

  TextWithDescription(this._mainText, this._descriptionText);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _mainTextWidget,
        _descriptionTextWidget  
      ],
    );
  }

  Widget get _mainTextWidget {
    return Container(
      padding: const EdgeInsets.only(bottom: 8),
      child: Text(
        _mainText,
        style: TextStyle(
          fontWeight: FontWeight.bold
        ),
      )
    );
  }

  Widget get _descriptionTextWidget {
    return Text(
      _descriptionText,
      style: TextStyle(
        color: Colors.grey[500]
      ),
    );
  }
  
}